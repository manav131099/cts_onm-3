rm(list = ls())
errHandle = file('/home/admin/Logs/LogsMY006SMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
system('rm -R "/home/admin/Dropbox/Second Gen/[MY-006S]"')
APPENDWARNING = 0
source('/home/admin/CODE/MY006SDigest/runHistory729.R')
require('mailR')
print('History done')
source('/home/admin/CODE/MY006SDigest/3rdGenData729.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/MY006SDigest/aggregateInfo.R')
print('3rd gen data called')
PR1 = 0
PR2 = 0
MYLD = vector()	
timetomins = function(x)	
{	
hr = unlist(strsplit(x,":"))	
min = as.numeric(hr[2])	
hr = as.numeric(hr[1])	
return((hr * 60 + min + 1))	
}	
rf = function(x)	
{	
return(format(round(x,2),nsmall=2))	
}	
rf1 = function(x)	
{	
return(format(round(x,1),nsmall=1))	
}	
rf3 = function(x)	
{	
return(format(round(x,3),nsmall=3))	
}	
checkdir = function(x)	
{	
if(!file.exists(x))	
{	
dir.create(x)	
}	
}	
DOB2 = "2019-11-30"	
DOB2=as.Date(DOB2)	
preparebody = function(path)	
{	
print("Enter Body Func")	
body=""	
body = paste(body,"Site Name: Coca-Cola Enstek\n",sep="")	
body = paste(body,"\nLocation: 1,  PT486, Persiaran Teknologi 4, Techpark@Enstek, 71760 Bandar Enstek, Negeri Sembilan\n")	
body = paste(body,"\nO&M Code: MY-006\n")	
body = paste(body,"\nSystem Size: 4053.3 kWp\n")	
body = paste(body,"\nNumber of Energy Meters: 3\n")	
body = paste(body, "\nSize of Meter-1 [kWp]: 1,615.6 (39.9%)\n")	
body = paste(body, "\nSize of Meter-2 [kWp]: 1,579.5 (39.0%)\n")	
body = paste(body, "\nSize of Meter-3 [kWp]: 858.2 (21.1%)\n")	
body = paste(body,"\nModule Brand / Model / Nos: JinkoSolar / JKM320PP-72-V / 5740\n")	
body = paste(body,"\nInverter Brand / Model / Nos:  SMA / SHP 75-10 / 46\n")	
body = paste(body,"\nSite COD: 2019-11-30\n")	
body = paste(body,"\nSystem age [days]:",((daysactive-30)),"\n")	
body = paste(body,"\nSystem age [years]:",round((daysactive-30)/365,2))	
for(x in 1 : length(path))	
{	
print(path[x])	
dataread = read.table(path[x],header = T,sep="\t",stringsAsFactors=F)	
body = paste(body,"\n\n_____________________________________________\n")	
days = unlist(strsplit(path[x],"/"))	
days = substr(days[length(days)],11,20)	
	yld21 = dataread[1,29]/1615.6
MYLD = c(MYLD, yld21)
yld22 = dataread[1,33]/1579.5
MYLD = c(MYLD, yld22)
yld23 = dataread[1,37]/858.2
MYLD = c(MYLD, yld23)
print("Printing MYLD")
print(MYLD)
if(length(MYLD))
{
MYLD = MYLD[complete.cases(MYLD)]
}
addwarn = 0
sddev = covar = NA
if(length(MYLD))
{
addwarn = 1
sddev = round(sdp(MYLD),2)
meanyld = mean(MYLD)
covar = round((sdp(MYLD)*100/meanyld),1)
}
body = paste(body,days)	
body  = paste(body,"\n_____________________________________________\n\n")	
body = paste(body,"Pts Recorded: ",as.character(dataread[1,2])," (",round(as.numeric(dataread[1,2])/14.4,1),"%)",sep="")	
body = paste(body,"\n\nDaily Irradiation Pyr [kWh/m2]:",as.character(dataread[1,3]))	
body = paste(body,"\n\nDaily Irradiation Si [kWh/m2]:",as.character(dataread[1,4]))	
body = paste(body,"\n\nPyr/Si ratio:",as.character(format(round(dataread[1,5],3), nsmall = 3)))	
body = paste(body,"\n\n=================")	
#body = paste(body,"\n\nFull Site Generation Method 1 [kWh]:",as.character(dataread[1,28]+dataread[1,32]+dataread[1,36]*-1))	
	body = paste(body,"\n\nFULL SITE Generation Method 1 [kWh]:",as.character(dataread[1,24]*-1))
body = paste(body,"\n\nFULL SITE Yield-1 [kWh/kWp]:",as.character((dataread[1,25]*-1)))
body = paste(body,"\n\nFULL SITE PR-1-Pyr [%]:",as.character(dataread[1,26]*-1))
body = paste(body,"\n\nFULL SITE PR-1-Si [%]:",as.character(dataread[1,27]*-1))
body = paste(body,"\n\nFULL SITE Generation Method 2 [kWh]:",as.character(dataread[1,40]))
body = paste(body,"\n\nFULL SITE Yield-2 [kWh/kWp]:",as.character((dataread[1,41])))
body = paste(body,"\n\nFULL SITE PR-2-Pyr [%]:",as.character(dataread[1,42]))
body = paste(body,"\n\nFULL SITE PR-2-Si [%]:",as.character(dataread[1,43]))
body = paste(body,"\n\nFULL SITE: Stdev/COV Yields: ",sddev," / ",covar,"%",sep="")
body = paste(body,"\n\n=================")	
body = paste(body,"\n\nEac-1 M1 [kWh]:",as.character(dataread[1,28]*-1))	
body = paste(body,"\n\nEac-2 M1 [kWh]:",as.character(dataread[1,29]))	
body = paste(body, "\n\nYield-1 M1 [kWh]:",round(dataread[1,28]*-1/1615.6,2))	
body = paste(body, "\n\nYield-2 M1 [kWh]:",round(dataread[1,29]/1615.6,2))	
	
PR1 = round(dataread[1,28]*-1*100/(1615.6*dataread[1,3]),1)	
PR2 = round(dataread[1,29]*100/(1615.6*dataread[1,3]),1)	
body = paste(body, "\n\nPR-1 M1 [%]:",PR1)	
body = paste(body, "\n\nPR-2 M1 [%]:",PR2)	
body = paste(body,"\n\nLast recorded M1 [kWh]:",as.character(dataread[1,30]))	
body = paste(body,"\n\nLast recorded time M1:",as.character(dataread[1,31]))	
body = paste(body,"\n\n=================")	
body = paste(body,"\n\nEac-1 M2 [kWh]:",as.character(dataread[1,32]*-1))	
body = paste(body,"\n\nEac-2 M2 [kWh]:",as.character(dataread[1,33]))	
body = paste(body, "\n\nYield-1 M2 [kWh]:",round(dataread[1,32]*-1/1579.5,2))	
body = paste(body, "\n\nYield-2 M2 [kWh]:",round(dataread[1,33]/1579.5,2))	
	
PR1 = round(dataread[1,32]*-1*100/(1579.5*dataread[1,3]),1)	
PR2 = round(dataread[1,33]*100/(1579.5*dataread[1,3]),1)	
body = paste(body, "\n\nPR-1 M2 [%]:",PR1)	
body = paste(body, "\n\nPR-2 M2 [%]:",PR2)	
body = paste(body,"\n\nLast recorded M2 [kWh]:",as.character(dataread[1,34]))	
body = paste(body,"\n\nLast recorded time M2:",as.character(dataread[1,35]))	
body = paste(body,"\n\n=================")	
body = paste(body,"\n\nEac-1 M3 [kWh]:",as.character(dataread[1,36]*-1))	
body = paste(body,"\n\nEac-2 M3 [kWh]:",as.character(dataread[1,37]))	
body = paste(body, "\n\nYield-1 M3 [kWh]:",round(dataread[1,36]*-1/858.2,2))	
body = paste(body, "\n\nYield-2 M3 [kWh]:",round(dataread[1,37]/858.2,2))	

PR1 = round(dataread[1,36]*-1*100/(858.2*dataread[1,3]),1)	
PR2 = round(dataread[1,37]*100/(858.2*dataread[1,3]),1)	
body = paste(body, "\n\nPR-1 M3 [%]:",PR1)	
body = paste(body, "\n\nPR-2 M3 [%]:",PR2)	
body = paste(body,"\n\nLast recorded M3 [kWh]:",as.character(dataread[1,38]))	
body = paste(body,"\n\nLast recorded time M1:",as.character(dataread[1,39]))	
body = paste(body,"\n\n=================")	
body = paste(body,"\n\nMean Tamb [C]:",as.character(dataread[1,6]))	
body = paste(body,"\n\nMean Tamb solar hours [C]:",as.character(dataread[1,9]))	
body = paste(body,"\n\nMax Tamb [C]:",as.character(dataread[1,7]))	
body = paste(body,"\n\nMean Tmod [C]:",as.character(dataread[1,18]))	
body = paste(body,"\n\nMean Tmod solar hours [C]:",as.character(dataread[1,21]))	
body = paste(body,"\n\nMax Tmod [C]:",as.character(dataread[1,19]))	
body = paste(body,"\n\nMean Hamb [%]:",as.character(dataread[1,12]))	
body = paste(body,"\n\nMean Hamb solar hours [%]:",as.character(dataread[1,15]))	
body  = paste(body,"\n\n_____________________________________________")	
}	
print('Daily Summary for Body Done')	

  body = paste(body,"\n\nStation History\n")
		body = paste(body,"\n_____________________________________________\n\n")

	body = paste(body,"Station Birth Date:",dobstation)
	body = paste(body,"\n\n# Days station active:",daysactive)
	body = paste(body,"\n\n# Years station active:",rf1((daysactive/365)))
print('3G Data Done')	
body = gsub("\n ","\n",body)
return(body)
}
path = "/home/admin/Dropbox/Cleantechsolar/1min/[729]"
pathwrite = "/home/admin/Dropbox/Second Gen/[MY-006S]"
checkdir(pathwrite)
date = format(Sys.time(),tz = "Singapore")
years = dir(path)
prevpathyear = paste(path,years[length(years)],sep="/")
months = dir(prevpathyear)
prevsumname = paste("[MY-006S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
prevpathmonth = paste(prevpathyear,months[length(months)],sep="/")
currday = prevday = dir(prevpathmonth)[length(dir(prevpathmonth))]
prevwriteyears = paste(pathwrite,years[length(years)],sep="/")
prevwritemonths = paste(prevwriteyears,months[length(months)],sep="/")
#if(length(dir(pathmonth)) > 1)
#{
#  prevday = dir(pathmonth[length(dir(pathmonth))-1])
#}
stnnickName = "729"
stnnickName2 = "MY-006S"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
if(!is.null(lastdatemail))
{
	yr = substr(lastdatemail,1,4)
	monyr = substr(lastdatemail,1,7)
	prevpathyear = paste(path,yr,sep="/")
	prevsumname = paste("[",stnnickName2,"] ",substr(lastdatemail,3,4),substr(lastdatemail,6,7),".txt",sep="")
	prevpathmonth = paste(prevpathyear,monyr,sep="/")
	currday = prevday = paste("[",stnnickName,"] ",lastdatemail,".txt",sep="")
	prevwriteyears = paste(pathwrite,yr,sep="/")
	prevwritemonths = paste(prevwriteyears,monyr,sep="/")
}

check = 0

sender <- "operations@cleantechsolar.com"
uname <- "shravan.karthik@cleantechsolar.com" 
pwd = 'CTS&*(789'
recipients <- getRecipients("MY-006S","m")
 
#"jeeva.govindarasu@cleantechsolar.com")

wt =1
idxdiff = 1
while(1)
{
recipients <- getRecipients("MY-006S","m")
recordTimeMaster("MY-006S","Bot")
years = dir(path)
writeyears = paste(pathwrite,years[length(years)],sep="/")
checkdir(writeyears)
pathyear = paste(path,years[length(years)],sep="/")
months = dir(pathyear)
writemonths = paste(writeyears,months[length(months)],sep="/")
sumname = paste("[MY-006S] ",substr(months[length(months)],3,4),substr(months[length(months)],6,7),".txt",sep="")
checkdir(writemonths)
pathmonth = paste(pathyear,months[length(months)],sep="/")
tm = timetomins(substr(format(Sys.time(),tz = "Singapore"),12,16))
currday = dir(pathmonth)[length(dir(pathmonth))]
if(prevday == currday)
{
   if(wt == 1)
{
 print("")
 print("")
 print("__________________________________________________________")
 print(substr(currday,7,16))
 print("__________________________________________________________")
 print("")
 print("")
 print('Waiting')
wt = 0
}
{
  if(tm > 1319 || tm < 30)
  {
      Sys.sleep(120)
  }        
  else
  {
    Sys.sleep(3600)
  }
}
next
}
print('New Data found.. sleeping to ensure sync complete')
Sys.sleep(60)
print('Sleep Done')
m1 = match(currday,dir(pathmonth)) 
m2 = match(prevday,dir(prevpathmonth))
idxdiff = m1 - m2
if(!is.finite(idxdiff))
{
print('############## Error in idx ####################')
print(paste('Currday',currday))
print(paste('pathmonth',pathmonth))
print(paste('prevday',prevday))
print(paste('prevpathmonth',prevpathmonth))
print(paste('m1',m1))
print(paste('m2',m2))
print('###############################################')
next	
}
if(idxdiff < 0)
{
	idxdiff = length(dir(prevpathmonth)) - match(prevday,dir(prevpathmonth)) + length(dir(pathmonth))
	newmonth = pathmonth
	newwritemonths = writemonths
	pathmonth = prevpathmonth
	writemonths = prevwritemonths
}
while(idxdiff)
{
{
if(prevday == dir(prevpathmonth)[length(dir(prevpathmonth))])
{
  pathmonth = newmonth
	writemonths = newwritemonths
  currday = dir(pathmonth)[1]
  monthlyirr1 = monthlyirr2 = monthlyirr3 = 0
  strng = unlist(strsplit(months[length(months)],"-"))
  daystruemonth = nodays(strng[1],strng[2])
  dayssofarmonth = 0
}	
else
currday = dir(pathmonth)[(match(prevday,dir(pathmonth)) +1)]
}
dataread = read.table(paste(pathmonth,currday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
datawrite = prepareSumm(dataread)
datasum = rewriteSumm(datawrite)
currdayw = gsub("729","MY-006S",currday)
write.table(datawrite,file = paste(writemonths,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)



#monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3])
#monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4])
#monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5])
#globirr1 = globirr1 + as.numeric(datawrite[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5])
#dayssofarmonth = dayssofarmonth + 1    
daysactive = daysactive + 1
#last30days[[last30daysidx]] = as.numeric(datawrite[1,4])
#last30days2[[last30daysidx]] = as.numeric(datawrite[1,5])



print('Digest Written');
{
  if(!file.exists(paste(writemonths,sumname,sep="/")))
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
    print('Summary file created')
}
  else 
  {
    write.table(datasum,file = paste(writemonths,sumname,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
   print('Summary file updated')  
}
}
filetosendpath = c(paste(writemonths,currdayw,sep="/"))
filename = c(currday)
filedesc = c("Daily Digest")
dataread = read.table(paste(prevpathmonth,prevday,sep='/'),header = T,sep = "\t",stringsAsFactors=F)
datawrite = prepareSumm(dataread)
prevdayw = gsub("729","MY-006S",prevday)
dataprev = read.table(paste(prevwritemonths,prevdayw,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
print('Summary read')
if(as.numeric(datawrite[,2]) != as.numeric(dataprev[,2]))
{
print('Difference in old file noted')



#previdx = (last30daysidx - 1) %% 31

#if(previdx == 0) {previdx = 1}
  datasum = rewriteSumm(datawrite)
  write.table(datawrite,file = paste(prevwritemonths,prevdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)


#if(prevpathmonth == pathmonth)
#{
#	monthlyirr1 = monthlyirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
#  monthlyirr2 = monthlyirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
#	monthlyirr3 = monthlyirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
#}
#globirr1 = globirr1 + as.numeric(datawrite[1,3]) - as.numeric(dataprev[1,3])
#globirr2 = globirr2 + as.numeric(datawrite[1,4]) - as.numeric(dataprev[1,4])
#globirr3 = globirr3 + as.numeric(datawrite[1,5]) - as.numeric(dataprev[1,5])
#last30days[[previdx]] = as.numeric(datawrite[1,4])
#last30days2[[previdx]] = as.numeric(datawrite[1,5])



datarw = read.table(paste(prevwritemonths,prevsumname,sep="/"),header = T,sep = "\t",stringsAsFactors=F)
  rn = match(as.character(datawrite[,1]),as.character(datarw[,1]))
  print(paste('Match found at row no',rn))
  datarw[rn,] = datasum[1,]
  write.table(datarw,file = paste(prevwritemonths,prevsumname,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
  filetosendpath[2] = paste(prevwritemonths,prevdayw,sep="/")
  filename[2] = prevday
  filedesc[2] = c("Updated Digest")
print('Updated old files.. both digest and summary file')
}

#last30daysidx = (last30daysidx + 1 ) %% 31
#if(last30daysidx == 0) {last30daysidx = 1}

#LAST30DAYSTOT = rf(sum(last30days))
#LAST30DAYSMEAN = rf(mean(last30days))
#LAST30DAYSTOT2 = rf(sum(last30days2))
#LAST30DAYSMEAN2 = rf(mean(last30days2))
#DAYSACTIVE = daysactive
#MONTHLYIRR1 = monthlyirr1
#MONTHLYIRR2 = monthlyirr2
#MONTHLYIRR3 = monthlyirr3
#MONTHAVG1 = rf(monthlyirr1 / dayssofarmonth)
#MONTHAVG2 = rf(monthlyirr2 / dayssofarmonth)
#MONTHAVG3 = rf(monthlyirr3 / dayssofarmonth)
#FORECASTIRR1 = rf(as.numeric(MONTHAVG1) * daystruemonth)
#FORECASTIRR2 = rf(as.numeric(MONTHAVG2) * daystruemonth)
#FORECASTIRR3 = rf(as.numeric(MONTHAVG3) * daystruemonth)
#SOILINGDEC = rf3(((globirr1 / globirr2) - 1) * 100)
#SNPDEC = rf3(((globirr3 / globirr2) - 1) * 100)
#SOILINGDECPD = rf3(as.numeric(SOILINGDEC) / DAYSACTIVE)
#SNPDECPD = rf3(as.numeric(SNPDEC) / DAYSACTIVE)

print('Sending mail')
body = ""
body = preparebody(filetosendpath)
send.mail(from = sender,
          to = recipients,
          subject = paste("Station [MY-006S] Digest",substr(currday,7,16)),
          body = body,
          smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
          authenticate = TRUE,
          send = TRUE,
          attach.files = filetosendpath,
          file.names = filename, # optional parameter
          debug = F)
print('Mail Sent');
recordTimeMaster("MY-006S","Mail",substr(currday,7,16))
prevday = currday
prevpathyear = pathyear
prevpathmonth = pathmonth
prevwriteyears = writeyears
prevwritemonths = writemonths
prevsumname = sumname
idxdiff = idxdiff - 1;
wt = 1
}
gc()
}
sink()
