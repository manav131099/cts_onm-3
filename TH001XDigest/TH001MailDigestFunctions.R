rm(list = ls())
source('/home/admin/CODE/TH001XDigest/Invoicing.R')
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
INSTCAP=c(1404)
TOTSOLAR = 0
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
getGTIData = function(day)
{
	yr = substr(day,1,4)
	yrmon = substr(day,1,7)
	path = paste("/home/admin/Dropbox/FlexiMC_Data/Second_Gen/[TH-001C]/",yr,"/",yrmon,"/WMS/[TH-001C]-WMS-",day,".txt",sep="")
	GTI = Tamb=Tmod=NA
	if(file.exists(path))
	{
		dataread = read.table(path,header = T,sep="\t")
		if(nrow(dataread))
		{
			GTI = as.numeric(dataread[1,3])
			Tamb = as.numeric(dataread[1,4])
			Tmod = as.numeric(dataread[1,5])
		}
	}
	return(c(GTI,Tamb,Tmod))
}
secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			TIMESTAMPSALARM2 <<- NULL
			TIMESTAMPSALARM3 <<- NULL
			}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPSALARM3 <<- NULL
		}
	}
	print(paste('IN 2G',filepath))
  dataread = try(read.table(filepath,header = T,sep = "\t",stringsAsFactors=F),silent=T)
	dataread2 = dataread
	{
	if(METERCALLED == 1 || METERCALLED == 2)
	{
		DA =date = Yld1 = Yld2 = DT = LastT = LastR = PR1 = PR2 = Eac1 = Eac2= GTI=TambVal=TmodVal=NA
		if((class(dataread2) != "try-error") && (nrow(dataread2)))
		{
			DA = round(nrow(dataread2)/14.4,1)
			date = substr(as.character(dataread[1,1]),1,10)
			idxpac = 19
			idxeac = 46
			dataread = dataread2[complete.cases(as.numeric(dataread2[,idxeac])),]
			if(nrow(dataread))
			{
				LastR = as.numeric(dataread[nrow(dataread),idxeac])/1000
				LastT = as.character(dataread[nrow(dataread),1])
				Eac2 = round(LastR - (as.numeric(dataread[1,idxeac])/1000),2)
			}
		
			dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
			if(nrow(dataread))
			{
			mins = timetomins(substr(as.character(dataread[,1]),12,16))
			len = dataread[(mins > 480),]
			mins = mins[mins>480]
			len = len[(mins < 1020),]
			noPts = nrow(len)
			if(noPts)
			{
				len = len[(as.numeric(len[,idxpac]) < 0.001),]
				if(nrow(len))
				{
					{
					if(METERCALLED == 1)
						TIMESTAMPSALARM <<- as.character(len[,1])
					else if(METERCALLED == 2)
						TIMESTAMPSALARM2 <<- as.character(len[,1])
					}
				}
			DT = round((540 - (noPts - nrow(len)))/5.4,1)
			}
			if(nrow(dataread))
			{
				Eac1 = round(sum(as.numeric(dataread[,idxpac]))/60000,2)
			}
			}
			Yld1 = round(Eac1/INSTCAP,2)
			Yld2 = round(Eac2/INSTCAP,2)
			GTI = getGTIData(date)
			TambVal = GTI[2]
			TmodVal = GTI[3]
			GTI = GTI[1]
			PR1 = round(Yld1*100/GTI,1)
			PR2 = round(Yld2*100/GTI,1)
	  }
	df = data.frame(Date = date, DA = DA, DT = DT,Eac1 = Eac1, Eac2 = Eac2,
	Yld1 = Yld1, Yld2 = Yld2, PR1 = PR1, PR2 = PR2, LastRead = LastR,
	LastTime = LastT,GTI=GTI,Tamb=TambVal,Tmod=TmodVal,stringsAsFactors=F)
 }
 else if(METERCALLED == 3)
 {
		DA = date = DT = Eac1 = GA = NA
		if((class(dataread2) != "try-error") && (nrow(dataread2)))
		{
		DA = round(nrow(dataread2)/14.4,1)
		date = substr(as.character(dataread[1,1]),1,10)
		idxpac = 12
		
		dataread = dataread2[complete.cases(as.numeric(dataread2[,idxpac])),]
		mins = timetomins(substr(as.character(dataread[,1]),12,16))
		len = dataread[(mins > 480),]
		mins = mins[mins>480]
		len = len[(mins < 1020),]
		noPts = nrow(len)
		len = len[(as.numeric(len[,idxpac]) < 0.001),]
		if(nrow(len))
		{
				TIMESTAMPSALARM3 <<- as.character(len[,1])
		}
		DT = round((540 - (noPts - nrow(len)))/5.4,1)
		if(nrow(dataread))
		{
			Eac1 = round(sum(as.numeric(dataread[,idxpac]))/60000,2)
		}
		dataread = dataread2[complete.cases(as.numeric(dataread2[,c(5)])),]
		dataread = dataread[complete.cases(as.numeric(dataread2[,c(6)])),]
		dataread = dataread[complete.cases(as.numeric(dataread2[,c(7)])),]

		c5 = as.numeric(dataread[,5])
		c6 = as.numeric(dataread[,6])
		c7 = as.numeric(dataread[,7])
		condn = (c5 <100000) || (c6 < 100000) || (c7 < 100000)
		dataread = dataread[condn,]
		GA = round(((1-(nrow(dataread)/max(nrow(dataread2),1)))*100),1)
 		}
	df = data.frame(Date = date, DA = DA, DT = DT,Eac1 = Eac1,GridAvail = GA,stringsAsFactors=F)
 }
 }
 write.table(df,file=writefilepath,sep="\t",row.names = F,col.names = T,append =F)
 return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,writefilepath,invoicingData)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	EacLoad1 = as.numeric(dataread3[,4])
	EacSol1Meth2 = as.numeric(dataread1[,5])
	EacSol2Meth2 = as.numeric(dataread2[,5])
  LastReadSol1 = as.numeric(dataread1[,10])
  LastReadSol2 = as.numeric(dataread2[,10])
	
	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DT = as.character(dataread1[,4]),
	DA = as.character(dataread1[,5]),
	GridAvail = as.numeric(dataread3[,5]),
	EacLoad1 = as.numeric(dataread3[,4]),
	EacSol1Meth1 = as.numeric(dataread1[,4]),
	EacSol1Meth2 = as.numeric(dataread1[,5]),
	EacSol2Meth1 = as.numeric(dataread2[,4]),
	EacSol2Meth2 = as.numeric(dataread2[,5]),
	LastReadSol1 = as.numeric(dataread1[,10]),
	LastTimeSol1 = as.numeric(dataread1[,11]),
	LastReadSol2 = as.numeric(dataread2[,10]),
	LastTimeSol2 = as.numeric(dataread2[,11]),
	GsiTot = as.numeric(dataread1[,12]),
	PR1Sol1 = as.numeric(dataread1[,8]),
	PR2Sol1 = as.numeric(dataread1[,9]),
	PR1Sol2 = as.numeric(dataread2[,8]),
	PR2Sol2 = as.numeric(dataread2[,9]),
	Yld1Sol1 = as.numeric(dataread1[,6]),
	Yld1Sol1 = as.numeric(dataread1[,7]),
	Yld2Load1 = as.numeric(dataread2[,6]),
	Yld2Load2 = as.numeric(dataread2[,7]),
	Tamb= as.numeric(dataread1[,13]),
	ArtLoad = EacLoad1 + EacSol1Meth2, 
	PercSol= round((EacSol1Meth2*100)/(EacSol1Meth2+EacLoad1),1),
	MtDiff = round(100*(LastReadSol1 - LastReadSol2)/LastReadSol1,3),
	RdDiff = round(100*(EacSol1Meth2- EacSol2Meth2)/EacSol1Meth2,3),
	Tmod= as.numeric(dataread1[,14]),
	PeakEac=as.numeric(invoicingData[,8]),
	OffPeakEac=as.numeric(invoicingData[,9]),
	PeakEacPerc = as.numeric(invoicingData[,10]),
	OffPeakEacPerc = as.numeric(invoicingData[,11]),
	Day = as.character(invoicingData[,2]),
	Holiday=as.character(invoicingData[,3]),
	stringsAsFactors=F)

  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
	return(df)
}

