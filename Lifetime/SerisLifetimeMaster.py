import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast

tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/Second Gen/'
path2='/home/admin/Dropbox/Lifetime/Gen-1/'
lifetimepath='/home/admin/CODE/Lifetime/Lifetime.csv'

while(1):
    dflifetime=pd.read_csv(lifetimepath)
    stations=dflifetime['Seris_Station_Name'].dropna().tolist()
    metercols=[]
    metercols2=[]
    b=dflifetime['Meter_Cols'].dropna().tolist()
    for i in b:
        temp=ast.literal_eval(i)
        metercols.append(temp)
    c=dflifetime['Meter_Cols2'].dropna().tolist()
    for i in c:
        temp=ast.literal_eval(i)
        metercols2.append(temp)
    nometers=dflifetime['Seris_No_Meters'].dropna().tolist()
    nometers=list(map(int, nometers))
    print(stations)
    print(metercols)
    print(metercols2)
    print(nometers)
    for index,n in enumerate(stations):
        if(os.path.exists(path2+n[1:7]+"-LT.txt")):
            pass
        else:
            for i in sorted(os.listdir(path+n)):
                for j in sorted(os.listdir(path+n+'/'+i)):
                    for k in sorted(os.listdir(path+n+'/'+i+'/'+j)):
                        if(len(k)==18):
                            df=pd.read_csv(path+n+'/'+i+'/'+j+'/'+k,sep='\t')
                            df1 = df[metercols[index]].copy()
                            df1.columns=metercols2[index]
                            print(df1)
                            df1.insert(2, "GHI-Flag", 0)
                            for m in range(int(nometers[index])):
                                print(type(m))
                            for m in range(int(nometers[index])):
                                df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
                            for m in range(int(nometers[index])):
                                df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
                            df1['Master-Flag']=0
                            if(os.path.exists(path2+n[1:7]+"-LT.txt")):
                                df1.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                            else:
                                df1.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-3.5)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    timenowdate2=timenow.strftime("%d-%m-%Y")
    print(timenowdate)

    for index,n in enumerate(stations):
        print(n)
        for i in range(1,15):
            df2=pd.read_csv(path2+n[1:7]+"-LT.txt",sep='\t')
            pastdate=timenow+datetime.timedelta(days=-i)
            pastdatestr=str(pastdate)
            pastdatestr2=pastdate.strftime("%d-%m-%Y")
            if(os.path.exists(path+n+'/'+pastdatestr[0:4]+'/'+pastdatestr[0:7]+'/'+n+' '+pastdatestr[2:4]+pastdatestr[5:7]+'.txt')):
                df=pd.read_csv(path+n+'/'+pastdatestr[0:4]+'/'+pastdatestr[0:7]+'/'+n+' '+pastdatestr[2:4]+pastdatestr[5:7]+'.txt',sep='\t')
            else:
                continue
            df1 = df[metercols[index]].copy()
            df1.columns=metercols2[index]
            #Flags
            df1.insert(2, "GHI-Flag", 0)
            for m in range(int(nometers[index])):
                df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
            for m in range(int(nometers[index])):
                df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
            df1['Master-Flag']=0
            if(((df2['Date']==pastdatestr[0:10]) & (df2['Master-Flag'].astype(str).str[-1]=='#')).any() or ((df2['Date']==pastdatestr2[0:10]) & (df2['Master-Flag'].astype(str).str[-1]=='#')).any()):
                print('SKIPPED!')
                pass
            elif(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                try:
                    df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
                    vals=df3.values.tolist()
                    df2.loc[(df2['Date']==pastdatestr[0:10])|(df2['Date']==pastdatestr2[0:10]),df2.columns.tolist()]=vals[0]
                    df2.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                except:
                    pass
            else:
                print('ABSENT')
                df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
                if(df3.empty):
                    pass
                else:
                    df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
        if(os.path.exists(path+n+'/'+timenowstr[0:4]+'/'+timenowstr[0:7]+'/'+n+' '+timenowstr[2:4]+timenowstr[5:7]+'.txt')):
            df=pd.read_csv(path+n+'/'+timenowstr[0:4]+'/'+timenowstr[0:7]+'/'+n+' '+timenowstr[2:4]+timenowstr[5:7]+'.txt',sep='\t')
        else:
            time.sleep(3600)
            continue
        df1 = df[metercols[index]].copy()
        df1.columns=metercols2[index]
        #Flags
        df1.insert(2, "GHI-Flag", 0)
        for m in range(int(nometers[index])):
            df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
        for m in range(int(nometers[index])):
            df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
        df1['Master-Flag']=0

        if((df2['Date']==timenowdate).any() or (df2['Date']==timenowdate2).any()):
            print("Skipped")
            pass
        else:
            df3=df1.loc[df1['Date']==timenowdate]
            if(df3.empty):
                print("Waiting")
            else:
                print('Writing')
                df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
    time.sleep(3600)

