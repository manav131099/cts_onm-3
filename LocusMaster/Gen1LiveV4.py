import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"
											
def authenticate():
    client_auth = requests.auth.HTTPBasicAuth(clientid, clientsecret)
    post_data = {"grant_type" : "password","username" : "shravan1994@gmail.com","password" : "welcome",
                 "client_id" : clientid,"client_secret" : clientsecret}
    												
    response = requests.post(accessurl,auth=client_auth,data=post_data)
    token_json = response.json()   							 
    return token_json["access_token"]  

 
	
def bot(siteid,sitename,pytimezone,timezone,coddate):
    while True:
        try:
            a=authenticate()
        except:
            print("Authentication API Failed!")
            time.sleep(5)
            continue
        break 
    def monthdelta(date, delta):
        m, y = (date.month+delta) % 12, date.year + ((date.month)+delta-1) // 12
        if not m: m = 12
        d = min(date.day, [31,
            29 if y%4==0 and not y%400==0 else 28,31,30,31,30,31,31,30,31,30,31][m-1])
        return date.replace(day=d,month=m, year=y)
    strtmnth=str(monthdelta(datetime.datetime.now().date(),-1))
    tz = pytz.timezone(pytimezone)					 
    stations = [[] for _ in range(3)]
    stationname = {}
    siteId = siteid
    componenturl ="https://api.locusenergy.com/v3/sites/"+ str(siteId) +"/components"
    while True:
        try:
            r = requests.get(componenturl, headers={'Authorization': "Bearer "+a})
            data2=r.json()
            temp=data2['components']
        except:
            print("Component API Failed!")
            time.sleep(5)
            continue
        break  
    for i in data2['components']:
        if((i['nodeType']=='METER' or i['nodeType']=='TRANSFORMER') and (i['isConceptualNode']==False) and ('Locus' not in i['name'])):
            stations[0].append(i['id'])
            stationname[i['id']]=i['name']
        elif(i['nodeType']=='INVERTER' and (i['isConceptualNode']==False) and ('Locus' not in i['name'])):
            stations[1].append(i['id'])
            stationname[i['id']]=i['name']
        elif(i['nodeType']=='WEATHERSTATION' and (i['isConceptualNode']==False) and ('Locus' not in i['name'])):
            stations[2].append(i['id'])
            stationname[i['id']]=i['name']

    #Getting the columns
    fin=[]
    for i in range(3):
        if(len(stations[i])!=0):
            fin=fin+[stations[i][0]]
    cols_arr=['ts','ts','ts']
    while True:
        try:
            for meter_index,s in enumerate(fin):
                k=[]
                temp=''
                stationurl ="https://api.locusenergy.com/v3/components/"+str(s)+"/dataavailable"
                r = requests.get(stationurl, headers={'Authorization': "Bearer "+a})
                data3=r.json()
                for i in data3['baseFields']:
                    for j in i['aggregations']:
                        k.append(j['shortName'])
                temp=','.join(k)
                cols_arr[meter_index]=temp
        except:
            print("Authentication Failed!")
            time.sleep(5)
            continue
        break   


    startpath="/home/admin/Start/"
    path="/home/admin/Dropbox/Gen 1 Data/"+sitename
    
    def chkdir(path):
        a=os.path.isdir(path)
        if(a):
            return
        else:
            os.makedirs(path)
            
    if(os.path.exists(startpath+sitename[1:(len(sitename)-1)]+".txt")):
        pass
    else:
        try:
            shutil.rmtree(path,ignore_errors=True)
        except Exception as e:
            print(e)
        with open(startpath+sitename[1:(len(sitename)-1)]+".txt", "w") as file:
            file.write(coddate+"\n00:00:00")   
    with open(startpath+sitename[1:(len(sitename)-1)]+".txt") as f:
        startdate = f.readline(10)
    
    chkdir(path)
    print(("Startdate is ",startdate))
    start=startdate
    start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
    final=start
    end=datetime.datetime.now(tz).date()
    tot=end-start
    tot=tot.days
    historicalflag=0
    print("Bot Started!")
    for key,i in enumerate(stations):
        while True:
            try:
                a=authenticate()
            except:
                print("Authentication Failed!")
                time.sleep(5)
                continue
            break  
        for key2,j in enumerate(i):
            start=startdate
            start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
            for k in range(0,tot):
                historicalflag=1
                end=start+datetime.timedelta(days=1)
                start2=str(start)
                temppath=path+'/'+start2[0:4]+'/'+start2[0:7]
                chkdir(temppath)
                if(key==0):
                    path2=temppath+'/'+'MFM_'+str(key2+1)+'_'+stationname[j]
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+cols_arr[0]
                elif(key==1):
                    path2=temppath+'/INVERTER'+'_'+str(key2+1)
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+cols_arr[1]
                else:
                    path2=temppath+'/'+'WMS'+'_'+str(key2+1)+'_'+stationname[j]
                    chkdir(path2)
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"T00:00:00&end="+str(end)+"T00:00:00&tz="+timezone+"&gran=5min&fields="+cols_arr[2]
                while True:
                    try:
                        r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                        data4=r.json()
                        dataframe=pd.DataFrame(data4['data'])
                    except:
                        print("Historical Data API Failed!")
                        time.sleep(5)
                        continue
                    break      
                dataframe['ts']=dataframe['ts'].str.replace('T',' ')
                dataframe.ts=dataframe.ts.str[0:19]
                col = dataframe.pop("ts")
                dataframe.insert(0, col.name, col)
                if(key==0):
                    dataframe.to_csv(path2+'/'+sitename+'-'+'MFM'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                elif(key==1):
                    dataframe.to_csv(path2+'/'+sitename+'-'+'I'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                else:
                    dataframe.to_csv(path2+'/'+sitename+'-'+'WMS'+str(key2+1)+'-'+str(start)+".txt",sep='\t',index=False)
                start=start+datetime.timedelta(days=1)
                final=start
    
    if(historicalflag==0):
        print("Historical Skipped!")
    else:
        print("Historical Done!") 
    
    def getcompid(stnname):
        l=stnname.split('_')
        if(l[0]=='INVERTER'):
            return stations[1][int(l[1])-1]
        elif(l[0]=='MFM'):
            return stations[0][int(l[1])-1]
        elif(l[0]=='WMS'):
            return stations[2][int(l[1])-1]
            
    def createstationstructure(finalpath,prevpath):
        for x in os.listdir(prevpath):
            chkdir(finalpath+'/'+x)
    
    def update(path,start,end):
        while True:
            try:
                a=authenticate()
            except:
                time.sleep(5)
                continue
            break  
        start=start.replace(minute=0,second=0)
        start=start.strftime("%Y-%m-%dT%H:%M:%S")
        end=end.replace(minute=0,second=0)
        end=end.strftime("%Y-%m-%dT%H:%M:%S")
        print('Adding data between',start,'and',end)
        for x in os.listdir(path):
            flag2=1
            for y in os.listdir(path+'/'+x):
                if(re.search(start[0:10],y)):
                    flag3=1
                    j=getcompid(x)
                    l=x.split('_')
                    if(l[0]=='INVERTER'): 
                        componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[1]
                    elif(l[0]=='MFM'): 
                        componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[0]
                    elif(l[0]=='WMS'):
                        componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[2]
                    while True:
                        try:
                            r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                            data4=r.json()
                            dataframe1=pd.DataFrame(data4['data'])
                        except Exception as e:
                            print(e)
                            print("Live update API Failed!")
                            time.sleep(5)
                            continue
                        break               
                    dataframe1['ts']=dataframe1['ts'].str.replace('T',' ')
                    dataframe1.ts=dataframe1.ts.str[0:19]
                    col = dataframe1.pop("ts")
                    dataframe1.insert(0, col.name, col)
                    if(end==end[0:11]+"23:00:00"):
                        flag3=0
                        print("Getting all data again from 11 ")
                        if(l[0]=='INVERTER'): 
                            componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+end[0:11]+"00:00:00&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[1]
                        elif(l[0]=='MFM'): 
                            componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+end[0:11]+"00:00:00&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[0]
                        elif(l[0]=='WMS'):
                            componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+end[0:11]+"00:00:00&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[2]
                        while True:
                            try:
                                r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                                data4=r.json()
                                dataframe1=pd.DataFrame(data4['data'])
                            except Exception as e:
                                print(e)
                                print("Live Data API Failed!")
                                time.sleep(5)
                                continue
                            break                 
                        dataframe1['ts']=dataframe1['ts'].str.replace('T',' ')
                        dataframe1.ts=dataframe1.ts.str[0:19]
                        col = dataframe1.pop("ts")
                        dataframe1.insert(0, col.name, col)
                    if(historicalflag==0 or flag3==0):#In case of failure and you run it again the same day.
                        with open(path+'/'+x+'/'+y, 'w') as f:
                            dataframe1.to_csv(f, header=True,sep='\t',index=False)
                    else:
                        with open(path+'/'+x+'/'+y, 'a') as f:
                            dataframe1.to_csv(f, header=False,sep='\t',index=False)
                    flag2=0
            if(flag2==1):
                j=getcompid(x)
                l=x.split('_')
                if(l[0]=='INVERTER'): 
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[1]
                elif(l[0]=='MFM'): 
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[0]
                elif(l[0]=='WMS'):
                    componenturl3="https://api.locusenergy.com/v3/components/"+str(j)+"/data?start="+str(start)+"&end="+str(end)+"&tz="+timezone+"&gran=5min&fields="+cols_arr[2]
                while True:
                    try:
                        r = requests.get(componenturl3, headers={'Authorization': "Bearer "+a})
                        data4=r.json()
                        dataframe=pd.DataFrame(data4['data'])
                    except Exception as e:
                        print(e)
                        print("Live update API Failed!")
                        time.sleep(5)
                        continue
                    break                
                dataframe['ts']=dataframe['ts'].str.replace('T',' ')
                dataframe.ts=dataframe.ts.str[0:19]
                col = dataframe.pop("ts")
                dataframe.insert(0, col.name, col)
                if(l[0]=='INVERTER'): 
                    b=str(start)
                    dataframe.to_csv(path+'/'+x+'/'+sitename+'-'+'I'+l[1]+'-'+b[0:10]+".txt",header=True,sep='\t',index=False)
                elif(l[0]=='MFM'): 
                    b=str(start)
                    dataframe.to_csv(path+'/'+x+'/'+sitename+'-'+'MFM'+l[1]+'-'+b[0:10]+".txt",header=True,sep='\t',index=False)
                elif(l[0]=='WMS'):
                    b=str(start)
                    dataframe.to_csv(path+'/'+x+'/'+sitename+'-'+'WMS'+l[1]+'-'+b[0:10]+".txt",header=True,sep='\t',index=False)
        currtime=datetime.datetime.now(tz)
        currtime=curr.replace(tzinfo=None)
        with open(startpath+"MasterMail/"+sitename[1:(len(sitename)-1)]+"_FTPNewFiles.txt", "w") as file:
            file.write(str(currtime))  
    
    
    flag=0
    print("RUNNING LIVE!")
    starttime=time.time()
    while(1):
        if(flag==0):
            start=str(final)
            start=datetime.datetime.strptime(start, "%Y-%m-%d")
        c=str(start)
        temppath=path+'/'+c[0:4]+'/'+c[0:7]
        curr=datetime.datetime.now(tz)
        curr = curr.replace(tzinfo=None)
        print('Time now is',str(curr))
        with open(startpath+"MasterMail/"+sitename[1:(len(sitename)-1)]+"_FTPProbe.txt", "w") as file:
            file.write(str(curr))  
        d=str(curr)
        if(d[8:10]=='01'):#New month so create structure.
            chkdir(path+'/'+d[0:4]+'/'+d[0:7])
            createstationstructure(path+'/'+d[0:4]+'/'+d[0:7],path+'/'+c[0:4]+'/'+c[0:7])
            prevmonth=str(monthdelta(curr,-1))
            createstationstructure(path+'/'+d[0:4]+'/'+d[0:7],path+'/'+prevmonth[0:4]+'/'+prevmonth[0:7])
        with open(startpath+sitename[1:(len(sitename)-1)]+".txt", "w") as file:
            file.write(str(curr.date())+"\n"+str(curr.time().replace(microsecond=0)))
        diff=curr-start
        diff=diff.seconds/3600
        if(diff<1 and flag==0):
            print("WAITING!")
        else:
            update(temppath,start,curr)
        start=curr
        flag=1
        historicalflag=1
        print('sleeping for an hour')
        time.sleep(3600.0 - ((time.time() - starttime) % 3600.0))

       
 
siteid=sys.argv[1]
sitename=sys.argv[2]
pytimezone=sys.argv[3]
timezone=sys.argv[4]
coddate=sys.argv[5]
bot(siteid,sitename,pytimezone,timezone,coddate)
   



