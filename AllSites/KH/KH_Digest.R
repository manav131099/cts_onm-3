rm(list = ls())
handle1=file("/home/admin/Logs/Logs-KH_Market_Digest.txt",open='wt',encoding='UTF-8')
handle="/home/admin/Dropbox/Second Gen/"
sitesumm="/home/admin/CODE/AllSites/KH/KH_Allsite.csv"
sink(handle1,type='message',append = T)
library(mailR)
source('/home/admin/CODE/common/math.R')

mailbody=function(sa,handle,sitesumm)
{
  atn=c()
  e=c()
  site=c()
  sitename=c()
  syssize=c()
  s=as.Date(sa)
  message(paste("Enter for Digest for ",as.character(s)))
  atn=0
  dz<-read.csv(sitesumm,header = T,encoding = "UTF-8")
  for (qw in 1:(nrow(dz))) 
  {
    site=c(site,as.character(dz[qw,1]))
    sitename=c(sitename,as.character(dz[qw,2]))
    syssize=c(syssize,as.numeric(dz[qw,3]))
  }
  body=""
  body = paste(body,"Market: Cambodia",sep="")
  body = paste(body,"\n\nDate: ",s,"\n\n",sep="")
  body = paste(body,"Number of Systems: ",nrow(dz)-1,sep="")
  for (rt in 1:nrow(dz))
  {
    path=handle
    if(as.character(site[rt])=="KH-001S" || as.character(site[rt])=="KH-002X" || as.character(site[rt])=="KH-003S" || as.character(site[rt])=="KH-003X" || as.character(site[rt])=="KH-005X" || as.character(site[rt])=="KH-006X")
    {
      if(as.character(site[rt])=="KH-001S" )
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-001S]","/",sep="")
        path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
        names=list.files(path)
        if(length(names)>0)
        {
          pat=grepl(s,names)
          n1=NA
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          path=paste(path,n1,sep="")
          if(file.exists(path))
          {
            df<-read.table(path,header = TRUE,sep = "\t")
            eac2=as.numeric(df[1,8])
            yld=round(as.numeric(df[1,16]),2)
            pr=round(as.numeric(df[1,15]),1)
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(eac2),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(yld),sep="")
            body = paste(body,"\n\nPerfomance Ratio Method 2 [%]: ",as.character(pr),sep="")
            body = paste(body,"\n\nPercentage solar power pumped to grid [%]: ",as.character(df[,9]),sep="")
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
                atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      if(as.character(site[rt])=="KH-002X")
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-002X]","/",sep="")
        patha=paste(path,substr(s,1,4),"/",substr(s,1,7),"/Loads/",sep="")
        pathb=paste(path,substr(s,1,4),"/",substr(s,1,7),"/Solar/",sep="")
        names=list.files(patha)
        names1=list.files(pathb)
        message(patha)
        message(pathb)
        if(length(names)>0  && length(names1)>0)
        {
          pat=grepl(s,names)
          pat1=grepl(s,names1)
          n1=NA
          n2=NA
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          for(i in 1:length(pat1))
          {
            if(pat1[i]==TRUE)
            {
              n2=as.character(names1[i])
            }
          }
          patha=paste(patha,n1,sep="")
          pathb=paste(pathb,n2,sep="")
          if(file.exists(patha) && file.exists(pathb))
          {
            df<-read.table(patha,header = TRUE,sep = "\t")
            dp<-read.table(pathb,header = TRUE,sep = "\t")
            eac2=as.numeric(df[1,3])+as.numeric(dp[1,3])
            yld=mean(c(as.numeric(df[1,13]),as.numeric(dp[1,12])))
            pr=mean(c(as.numeric(df[1,14]),as.numeric(dp[1,13])))
            
            body = paste(body,"\n\n>>> Solar",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dp[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dp[1,12]),sep="")
            body = paste(body,"\n\nPerfomance Ratio Method 2 [%]: ",as.character(dp[1,13]),sep="")
            
            
            body = paste(body,"\n\n>>> Loads",sep="")
            
            body = paste(body,"\n\nSolar energy fed to the grid [kWh]:",as.character(df[,5]))
            perctouse = round((as.numeric(df[1,5])*100/as.numeric(dp[1,3])),1)
            body = paste(body,"\n\nPercentage solar energy fed to the grid [%]",as.character(perctouse))
            
            
            yldd=sdp(c(as.numeric(df[1,13]),as.numeric(dp[1,12])))
            prr=sdp(c(as.numeric(df[1,14]),as.numeric(dp[1,13])))
            ylddcv=(yldd)*100/yld
            prrcv=(prr*100)/pr
            body = paste(body,"\n\nStdDev [Yield]: ",formatC(yldd,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [Yield]: ",formatC(ylddcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            body = paste(body,"\n\nStdDev [PR]: ",formatC(prr,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [PR]: ",formatC(prrcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
               atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      if(as.character(site[rt])=="KH-003S" )
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-003S]","/",sep="")
        path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
        names=list.files(path)
        if(length(names)>0)
        {
          pat=grepl(s,names)
          n1=NA
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          path=paste(path,n1,sep="")
          if(file.exists(path))
          {
            df<-read.table(path,header = TRUE,sep = "\t")
            eac2=as.numeric(df[1,6])
            yld=round((as.numeric(df[1,6])/25.92/100),2)
            pr=round((as.numeric(df[1,6])/as.numeric(df[1,4])*25.92),1)
            InstCap=25.92
            PRSUBS2 = format(round((as.numeric(df[1,11])/(as.numeric(df[1,4])*InstCap)),1),nsmall=1)
            YLDSUBS2 = round((as.numeric(df[1,6])/InstCap/100),2)
            body = paste(body,"\n\nEac Substation - Method 2 [kWh]: ",as.character(df[,11]),sep="")
            body = paste(body,"\n\nYield Substation - Method 2 [kWh/kWp]: ",as.character(YLDSUBS2),sep="")
            body = paste(body,"\n\nPR Substation - Method 2 [%]: ",as.character(PRSUBS2),sep="")
            
            body = paste(body,"\n\nDaily Irradiation [kWh/m2]: ",as.character(df[1,4]),sep="")
            body = paste(body,"\n\nRoom Temparature [C]: ",as.character(df[1,16]),sep="")
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
                atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      if(as.character(site[rt])=="KH-003X" )
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-003X]","/",sep="")
        patha=paste(path,substr(s,1,4),"/",substr(s,1,7),"/CKL/",sep="")
        pathb=paste(path,substr(s,1,4),"/",substr(s,1,7),"/CKX/",sep="")
        pathc=paste(path,substr(s,1,4),"/",substr(s,1,7),"/SLX/",sep="")
        names=list.files(patha)
        names1=list.files(pathb)
        names2=list.files(pathc)
        if(length(names)>0 && length(names1)>0 && length(names2)>0)
        {
          pat=grepl(s,names)
          pat1=grepl(s,names1)
          pat2=grepl(s,names2)
          n1=NA
          n2=NA
          n3=NA
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          for(i in 1:length(pat1))
          {
            if(pat1[i]==TRUE)
            {
              n2=as.character(names1[i])
            }
          }
          for(i in 1:length(pat2))
          {
            if(pat2[i]==TRUE)
            {
              n3=as.character(names2[i])
            }
          }
          patha=paste(patha,n1,sep="")
          pathb=paste(pathb,n2,sep="")
          pathc=paste(pathc,n3,sep="")
          if(file.exists(patha) && file.exists(pathb) && file.exists(pathc))
          {
            df<-read.table(patha,header = TRUE,sep = "\t")
            dp<-read.table(pathb,header = TRUE,sep = "\t")
            dq<-read.table(pathc,header = TRUE,sep = "\t")
            eac2=as.numeric(df[1,2])+as.numeric(dp[1,3])+as.numeric(dq[1,3])
            yld=mean(c(as.numeric(df[1,5]),as.numeric(dp[1,11]),as.numeric(dq[1,13])))
            pr=mean(c(as.numeric(df[1,6]),as.numeric(dp[1,10]),as.numeric(dq[1,11])))
            
            body = paste(body,"\n>>> CKX",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dp[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dp[1,12]),sep="")
            body = paste(body,"\n\nPercentage loads powered by solar [%]: ",as.character(dp[1,10]),sep="")
            
            body = paste(body,"\n\n>>> SLX",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dq[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dq[1,12]),sep="")
            
            
            body = paste(body,"\n\n>>> CKL",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,3]),sep="")
            artLoad = (as.numeric(dp[1,2])+as.numeric(df[1,2]))
            solCont = round(as.numeric(dq[1,2]) * 100/artLoad,1)
            body = paste(body,"\n\nArtificial load [kWh]: ",artLoad,sep="")
            body = paste(body,"\n\nSolar contribution [%]: ",solCont,sep="")
            
            yldd=sdp(c(as.numeric(df[1,5]),as.numeric(dp[1,11]),as.numeric(dq[1,12])))
            prr=sdp(c(as.numeric(df[1,6]),as.numeric(dp[1,10]),as.numeric(dq[1,11])))
            ylddcv=(yldd)*100/yld
            prrcv=(prr*100)/pr
            body = paste(body,"\n\nStdDev [Yield]: ",formatC(yldd,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [Yield]: ",formatC(ylddcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            body = paste(body,"\n\nStdDev [PR]: ",formatC(prr,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [PR]: ",formatC(prrcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
                atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      if(as.character(site[rt])=="KH-004X")
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-004X]","/",sep="")
        patha=paste(path,substr(s,1,4),"/",substr(s,1,7),"/MSB-01 Loads/",sep="")
        pathb=paste(path,substr(s,1,4),"/",substr(s,1,7),"/MSB-02 Loads/",sep="")
        pathc=paste(path,substr(s,1,4),"/",substr(s,1,7),"/MSB-01 Solar/",sep="")
        pathd=paste(path,substr(s,1,4),"/",substr(s,1,7),"/MSB-02 Solar/",sep="")
        
        names=list.files(patha)
        names1=list.files(pathb)
        names2=list.files(pathc)
        names3=list.files(pathd)
        
        if(length(names)>0 && length(names1)>0 && length(names2)>0 && length(names3))
        {
          pat=grepl(s,names)
          pat1=grepl(s,names1)
          pat2=grepl(s,names2)
          pat3=grepl(s,names3)
          
          n1=NA
          n2=NA
          n3=NA
          n4=NA
          
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          for(i in 1:length(pat1))
          {
            if(pat1[i]==TRUE)
            {
              n2=as.character(names1[i])
            }
          }
          for(i in 1:length(pat2))
          {
            if(pat2[i]==TRUE)
            {
              n3=as.character(names2[i])
            }
          }
          for(i in 1:length(pat3))
          {
            if(pat3[i]==TRUE)
            {
              n4=as.character(names3[i])
            }
          }
          
          patha=paste(patha,n1,sep="")
          pathb=paste(pathb,n2,sep="")
          pathc=paste(pathc,n3,sep="")
          pathd=paste(pathd,n4,sep="")
          
          if(file.exists(patha) && file.exists(pathb) && file.exists(pathc) && file.exists(pathd)){
            df<-read.table(patha,header = TRUE,sep = "\t")
            dp<-read.table(pathb,header = TRUE,sep = "\t")
            dq<-read.table(pathc,header = TRUE,sep = "\t")
            dr<-read.table(pathd,header = TRUE,sep = "\t")
            
            
            eac2=as.numeric(df[1,2])+as.numeric(dp[1,2])+as.numeric(dq[1,3])+as.numeric(dr[1,3])
            yld=mean(c(as.numeric(df[1,5]),as.numeric(dp[1,5]),as.numeric(dq[1,10]),as.numeric(dr[1,10])))
            pr=mean(c(as.numeric(df[1,6]),as.numeric(dp[1,6]),as.numeric(dq[1,12]),as.numeric(dr[1,12])))
            
            FSGeneration = as.numeric(eac2)
            FSPR = round((FSGeneration/4.6654/as.numeric(df[1,7])),1)
            body = paste(body,"\n\nFull site generation [kWh]: ",as.character(round(FSGeneration,2)),sep="")
            body = paste(body,"\n\nFull site yield [kWh/kWp]: ",as.character(round(FSGeneration/1001,2)),sep="")
            body = paste(body,"\n\nFull site PR [%]: ",as.character(round(FSPR,1)),sep="")
            
            
            body = paste(body,"\n\n>>> MSB-01 Solar",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dq[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dq[1,10]),sep="")
            body = paste(body,"\n\nPerformance Ratio- 2 [%]: ",as.character(dq[1,12]),sep="")
            
            body = paste(body,"\n\n>>> MSB-02 Solar",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dr[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dr[1,10]),sep="")
            body = paste(body,"\n\nPerformance Ratio- 2 [%]: ",as.character(dr[1,12]),sep="")
            
            body = paste(body,"\n\n>>> MSB-01 Loads",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,2]),sep="")
            
            
            body = paste(body,"\n\n>>> MSB-02 Loads",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dp[1,2]),sep="")
            
            
            
            yldd=sdp(c(as.numeric(df[1,5]),as.numeric(dp[1,5]),as.numeric(dq[1,10]),as.numeric(dr[1,10])))
            prr=sdp(c(as.numeric(df[1,6]),as.numeric(dp[1,6]),as.numeric(dq[1,11]),as.numeric(dr[1,11])))
            ylddcv=(yldd)*100/yld
            prrcv=(prr*100)/pr
            body = paste(body,"\n\nStdDev [Yield]: ",formatC(yldd,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [Yield]: ",formatC(ylddcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            body = paste(body,"\n\nStdDev [PR]: ",formatC(prr,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [PR]: ",formatC(prrcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
                atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      if(as.character(site[rt])=="KH-005X")
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-005X]","/",sep="")
        patha=paste(path,substr(s,1,4),"/",substr(s,1,7),"/ComAp/",sep="")
        pathb=paste(path,substr(s,1,4),"/",substr(s,1,7),"/SCL/",sep="")
        pathc=paste(path,substr(s,1,4),"/",substr(s,1,7),"/SCS/",sep="")
        names=list.files(patha)
        names1=list.files(pathb)
        names2=list.files(pathc)
        if(length(names)>0 && length(names1)>0 && length(names2)>0)
        {
          pat=grepl(s,names)
          pat1=grepl(s,names1)
          pat2=grepl(s,names2)
          n1=NA
          n2=NA
          n3=NA
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          for(i in 1:length(pat1))
          {
            if(pat1[i]==TRUE)
            {
              n2=as.character(names1[i])
            }
          }
          for(i in 1:length(pat2))
          {
            if(pat2[i]==TRUE)
            {
              n3=as.character(names2[i])
            }
          }
          patha=paste(patha,n1,sep="")
          pathb=paste(pathb,n2,sep="")
          pathc=paste(pathc,n3,sep="")
          if(file.exists(patha) && file.exists(pathb) && file.exists(pathc))
          {
            df<-read.table(patha,header = TRUE,sep = "\t")
            dp<-read.table(pathb,header = TRUE,sep = "\t")
            dq<-read.table(pathc,header = TRUE,sep = "\t")
            eac2=as.numeric(df[1,2])+as.numeric(dp[1,3])+as.numeric(dq[1,3])
            yld=mean(c(as.numeric(df[1,5]),as.numeric(dp[1,11]),as.numeric(dq[1,13])))
            pr=mean(c(as.numeric(df[1,6]),as.numeric(dp[1,10]),as.numeric(dq[1,11])))
            
            body = paste(body,"\n>>> SCS",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dq[1,3]),sep="")
            body = paste(body,"\n\nPerformance Ration [%]: ",as.character(dq[1,11]),sep="")
            
            body = paste(body,"\n\n>>> ComAp",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,2]),sep="")
            body = paste(body,"\n\nPercentage loads powered by solar [%]: ",as.character(df[1,6]),sep="")
            
            body = paste(body,"\n\n>>> SCL",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dp[1,3]),sep="")
            body = paste(body,"\n\nPercentage loads powered by solar [%]: ",as.character(dp[1,10]),sep="")
            
            yldd=sdp(c(as.numeric(df[1,5]),as.numeric(dp[1,11]),as.numeric(dq[1,13])))
            prr=sdp(c(as.numeric(df[1,6]),as.numeric(dp[1,10]),as.numeric(dq[1,11])))
            ylddcv=(yldd)*100/yld
            prrcv=(prr*100)/pr
            body = paste(body,"\n\nStdDev [Yield]: ",formatC(yldd,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [Yield]: ",formatC(ylddcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            body = paste(body,"\n\nStdDev [PR]: ",formatC(prr,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [PR]: ",formatC(prrcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
                atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      if(as.character(site[rt])=="KH-006X")
      {
        body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
        body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
        body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
        path=paste(path,"[KH-006X]","/",sep="")
        patha=paste(path,substr(s,1,4),"/",substr(s,1,7),"/ComAp-01/",sep="")
        pathb=paste(path,substr(s,1,4),"/",substr(s,1,7),"/ComAp-02/",sep="")
        pathc=paste(path,substr(s,1,4),"/",substr(s,1,7),"/MSB-01-Solar/",sep="")
        pathd=paste(path,substr(s,1,4),"/",substr(s,1,7),"/MSB-02-Solar/",sep="")
        pathe=paste(path,substr(s,1,4),"/",substr(s,1,7),"/Sensors/",sep="")
        names=list.files(patha)
        names1=list.files(pathb)
        names2=list.files(pathc)
        names3=list.files(pathd)
        names4=list.files(pathe)
        if(length(names)>0 && length(names1)>0 && length(names2)>0 && length(names3))
        {
          pat=grepl(s,names)
          pat1=grepl(s,names1)
          pat2=grepl(s,names2)
          pat3=grepl(s,names3)
          pat4=grepl(s,names4)
          n1=NA
          n2=NA
          n3=NA
          n4=NA
          n5=NA
          for(i in 1:length(pat))
          {
            if(pat[i]==TRUE)
            {
              n1=as.character(names[i])
            }
          }
          for(i in 1:length(pat1))
          {
            if(pat1[i]==TRUE)
            {
              n2=as.character(names1[i])
            }
          }
          for(i in 1:length(pat2))
          {
            if(pat2[i]==TRUE)
            {
              n3=as.character(names2[i])
            }
          }
          for(i in 1:length(pat3))
          {
            if(pat3[i]==TRUE)
            {
              n4=as.character(names3[i])
            }
          }
          for(i in 1:length(pat4))
          {
            if(pat4[i]==TRUE)
            {
              n5=as.character(names4[i])
            }
          }
          patha=paste(patha,n1,sep="")
          pathb=paste(pathb,n2,sep="")
          pathc=paste(pathc,n3,sep="")
          pathd=paste(pathd,n4,sep="")
          pathe=paste(pathe,n5,sep="")
          if(file.exists(patha) && file.exists(pathb) && file.exists(pathc) && file.exists(pathd) && file.exists(pathe)){
            df<-read.table(patha,header = TRUE,sep = "\t")
            dp<-read.table(pathb,header = TRUE,sep = "\t")
            dq<-read.table(pathc,header = TRUE,sep = "\t")
            dr<-read.table(pathd,header = TRUE,sep = "\t")
            ds<-read.table(pathe,header = TRUE,sep = "\t")
            
            eac2=as.numeric(df[1,2])+as.numeric(dp[1,2])+as.numeric(dq[1,3])+as.numeric(dr[1,3])
            yld=mean(c(as.numeric(df[1,5]),as.numeric(dp[1,5]),as.numeric(dq[1,10]),as.numeric(dr[1,10])))
            pr=mean(c(as.numeric(df[1,7]),as.numeric(dp[1,6]),as.numeric(dq[1,12]),as.numeric(dr[1,12])))
            
            FSGeneration = eac2
            FSPR = round((FSGeneration/10.01/as.numeric(ds[1,2])),1)
            body = paste(body,"\n\nFull site generation [kWh]: ",as.character(round(FSGeneration,2)),sep="")
            body = paste(body,"\n\nFull site yield [kWh/kWp]: ",as.character(round(FSGeneration/1001,2)),sep="")
            body = paste(body,"\n\nFull site PR [%]: ",as.character(round(FSPR,1)),sep="")
            body = paste(body,"\n\nTotal daily irradiation [kWh/m2]: ",as.character(ds[1,2]),sep="")
            
            body = paste(body,"\n\n>>> MSB-01-Solar",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dq[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dq[1,10]),sep="")
            body = paste(body,"\n\nPerformance Ratio- 2 [%]: ",as.character(dq[1,11]),sep="")
            
            body = paste(body,"\n\n>>> MSB-02-Solar",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dr[1,3]),sep="")
            body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(dr[1,10]),sep="")
            body = paste(body,"\n\nPerformance Ratio- 2 [%]: ",as.character(dr[1,11]),sep="")
            
            body = paste(body,"\n\n>>> ComAp-01",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,2]),sep="")
            body = paste(body,"\n\nPerformance Ratio- 2 [%]: ",as.character(df[1,6]),sep="")
            
            body = paste(body,"\n\n>>> ComAp-02",sep="")
            
            body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(dp[1,2]),sep="")
            body = paste(body,"\n\nPerformance Ratio- 2 [%]: ",as.character(dp[1,6]),sep="")
            
            
            yldd=sdp(c(as.numeric(df[1,5]),as.numeric(dp[1,5]),as.numeric(dq[1,10]),as.numeric(dr[1,10])))
            prr=sdp(c(as.numeric(df[1,6]),as.numeric(dp[1,6]),as.numeric(dq[1,11]),as.numeric(dr[1,11])))
            ylddcv=(yldd)*100/yld
            prrcv=(prr*100)/pr
            body = paste(body,"\n\nStdDev [Yield]: ",formatC(yldd,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [Yield]: ",formatC(ylddcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            body = paste(body,"\n\nStdDev [PR]: ",formatC(prr,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
            body = paste(body,"\n\nCoV [PR]: ",formatC(prrcv,format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
            
            e=c(e,as.numeric(yld))
            if(!is.na(pr))
            {
              if(as.numeric(pr)<60)
                atn=c(atn,as.character(site[rt]))
            }
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
    }
    else  
    {
      body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
      body = paste(body,"\n",site[rt]," ", sitename[rt]," ", syssize[rt]," ", "kWp",sep="")
      body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
      path=paste(path,"[",site[rt],"]","/",sep="")
      path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
      dirs=c()
      dir=list.dirs(path,full.names = F,recursive = F)
      if(length(dir)>0)
      {
        for (i in 1:length(dir)) 
        {
          if(!grepl("^[.]",dir[i]))
          {
            dirs=c(dirs,dir[i])
          }
        }
        dirs=dirs[!is.na(dirs)]
        if(length(dirs)<1)
        {
          body = paste(body,"\n\nNo Data",sep="")
        }
        if(length(dirs)>0)
        {
          for (ij in 1:length(dirs)) 
          {
            path1=paste(path,as.character(dirs[ij]),"/",sep="")
            names=list.files(path1)
            
            if(length(names)>0)
            {
              pat=grepl(s,names)
              n1=NA
              for(i in 1:length(pat))
              {
                if(isTRUE(pat[i]))
                {
                  n1=as.character(names[i])
                }
              }
              path2=paste(path1,n1,sep="")
              if(file.exists(path2))
              {
                df<-read.table(path2,header = TRUE,sep = "\t")
                col=colnames(df)
                y1=grepl("Yld",col,ignore.case = T)
                y2=grepl("DailySpecYield",col,ignore.case = T)
                y3=grepl("DailySpecificYield",col,ignore.case = T)
                p1=grepl("PR",col,ignore.case = T)
                p2=grepl("PR2",col,ignore.case = T)
                ec1=grepl("Eac2",col,ignore.case = T)
                ec2=grepl("LoadEac",col,ignore.case = T)
                gh=gh1=gh2=demo=NA
                for (i in 1:length(y2)) 
                {
                  if(isTRUE(y3[i]))
                  {
                    gh=i
                  }
                  else
                  {
                    if(isTRUE(y2[i]))
                      gh=i
                    else
                    {
                      if(isTRUE(y1[i]))
                        gh=i
                    }
                  }
                }
                for(i in 1:length(p2))
                {
                  if(isTRUE(p2[i]))
                  {
                    demo=i
                  }
                }
                if(is.na(demo))
                {
                  for(i in 1:length(p1))
                  {
                    if(isTRUE(p1[i]))
                    {
                      gh1=i
                    }
                  }
                }
                else
                  gh1=demo
                for (i in 1:length(ec1)) 
                {
                  if(isTRUE(ec1[i]))
                  {
                    gh2=i
                  }
                  else
                  {
                    if(isTRUE(ec2[i]))
                      gh2=i
                  }
                }
                body = paste(body,"\n>>> ",dirs[ij],sep="")
                if(!is.na(gh2))
                  body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,gh2]),sep="")
                if(!is.na(gh))
                  body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(df[1,gh]),sep="")
                if(!is.na(gh1))
                {
                  body = paste(body,"\n\nPerfomance Ratio Method 2 [%]: ",as.character(df[1,gh1]),sep="")
                  atnChk = as.numeric(df[1,gh1])
                  if(!is.na(atnChk))
                  {
                    if(as.numeric(atnChk)<60)
                      atn=c(atn,as.character(site[rt]))
                  }
                }
                if(!is.na(gh))
                  e=c(e,as.numeric(df[1,gh]))
              }
              else
                body = paste(body,"\n\nData Not Available for ",dirs[ij],sep="")
            }
            else
              body = paste(body,"\n\nData Not Available for a Long Time for ",dirs[ij],sep="")
          }
        }
      }
      if(length(dir)<1)
      {
        names=list.files(path)
        
        if(length(names)>0)
        {
          pat=grepl(s,names)
          n1=NA
          for(i in 1:length(pat))
          {
            if(isTRUE(pat[i]))
            {
              n1=as.character(names[i])
            }
          }
          path1=paste(path,n1,sep="")
          
          if(file.exists(path1))
          {
            df<-read.table(path1,header = TRUE,sep = "\t")
            col=colnames(df)
            y1=grepl("Yld",col,ignore.case = T)
            y2=grepl("DailySpecYield",col,ignore.case = T)
            y3=grepl("DailySpecificYield",col,ignore.case = T)
            p1=grepl("PR",col,ignore.case = T)
            p2=grepl("PR2",col,ignore.case = T)
            ec1=grepl("Eac2",col,ignore.case = T)
            ec2=grepl("LoadEac",col,ignore.case = T)
            gh=gh1=gh2=demo=NA
            for (i in 1:length(y2)) 
            {
              if(isTRUE(y3[i]))
              {
                gh=i
              }
              else
              {
                if(isTRUE(y2[i]))
                  gh=i
                else
                {
                  if(isTRUE(y1[i]))
                    gh=i
                }
              }
            }
            for(i in 1:length(p2))
            {
              if(isTRUE(p2[i]))
              {
                demo=i
              }
            }
            if(is.na(demo))
            {
              for(i in 1:length(p1))
              {
                if(isTRUE(p1[i]))
                {
                  gh1=i
                }
              }
            }
            else
              gh1=demo
            for (i in 1:length(ec1)) 
            {
              if(isTRUE(ec1[i]))
              {
                gh2=i
              }
              else
              {
                if(isTRUE(ec2[i]))
                  gh2=i
              }
            }
            if(!is.na(gh2))
              body = paste(body,"\n\nEAC2 system generated power [kWh]: ",as.character(df[1,gh2]),sep="")
            if(!is.na(gh))
              body = paste(body,"\n\nDaily specific yield [kWh/kWp]: ",as.character(df[1,gh]),sep="")
            if(!is.na(gh1))
            {
              body = paste(body,"\n\nPerfomance Ratio Method 2 [%]: ",as.character(df[1,gh1]),sep="")
              atnChk = as.numeric(df[1,gh1])
              if(!is.na(atnChk))
              {
                if(as.numeric(atnChk)<60)
                  atn=c(atn,as.character(site[rt]))
              }
            }
            if(!is.na(gh))
              e=c(e,as.numeric(df[1,gh]))
          }
          else
            body = paste(body,"\n\nData Not Available",sep="")
        }
        else
          body = paste(body,"\n\nData Not Available for a Long Time",sep="")
      }
      
    }
  }
  body = paste(body,"\n\n---------------------------------------------------------------------------------",sep="")
  body = paste(body,"\n---------------------------------------------------------------------------------",sep="")
  e=e[!is.na(e)]
  body = paste(body,"\n\nStdDev [Yield]: ",formatC(sdp(e),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  body = paste(body,"\n\nCoV [Yield]: ",formatC((sdp(e)*100)/mean(e),format="f",digits = 1,big.mark = ",",big.interval = 3L)," [%]",sep="")
  body = paste(body,"\n\nInstalled Capacity [kWp]: ",formatC((sum(syssize)-2592),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  body = paste(body,"\n\nAverage System Size [kWp]: ",formatC(((sum(syssize)-2592)/6),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  emer=atn
  b=list(body,atn,emer)
  message("Mail Digest Done")
  return(b)
}


#Mail Section
sender <- "operations@cleantechsolar.com" 
uname <- 'shravan.karthik@cleantechsolar.com'
pwd = 'CTS&*(789'
recipients <- c("rupesh.baker@cleantechsolar.com","visal.yang@cleantechsolar.com",
"lucas.ferrand@cleantechsolar.com")
sendToken = 0
forceSendToken = 0

while (1) 
{ 
  datePrev=as.character(as.Date(substr(format(Sys.time(),tz="Singapore"),1,10))-1)
	hourNow = format(Sys.time(),tz = "Singapore")
	hrNow = as.numeric(substr(hourNow,12,13))
	if(hrNow == 1 || hrNow == 2)
		sendToken = 1
	if(((hrNow == 7 || hrNow == 8) && sendToken) || forceSendToken)
  {
    message(format(Sys.time(),tz=Sys.timezone()))
    message("Mail Section")
    body=mailbody(as.Date(datePrev),handle,sitesumm)
    if(body[[2]]==0)
      sub=paste("[",as.character(datePrev),"]"," KH All Site Summary", sep = "")
    if(body[[2]]>0)
      sub=paste("[",as.character(datePrev),"]"," <WARNING-PR for " ,as.character(body[[3]])," > KH All Site Summary", sep = "")
    send.mail(from = sender,
              to = recipients,
              subject = sub,
              body = as.character(body[[1]]),
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              debug = F)
    message("Mail Sent")
    lastmail=datePrev
    message(paste("Last mail sent for: ",lastmail,sep = ""))
    message("\n----------------------------------------------------------------")
		sendToken = 0
		forceSendToken=0
  }
    message("Going to sleep...")
    Sys.sleep(3600)
}
sink()
