checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf2 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}

prepareSumm = function(dataread)
{
  
  daycall = da = daPerc=gs1 = gsi2 = gsi3 = gsi4 = gsi5 = tamb = tambst = tambmx = tambmn = tambstmx = NA
  tambstmn = hamb = hambst = hambmx = hambmn = hambstmx = hambstmn = tsi1 = tsi2 = gsirat1 = gsirat2 = NA
  rowa0 = tsi1st = tsi2st = rowcj = trans = rowcjlast = timelast = TSGHV = SCHV = TSGHVTot = SCHVTot = SiteLoss =gmodwt= NA
  ga_lv=ga_hv=slv=shv=NA
  if(nrow(dataread)) 
  {
		offsetCols = 0
    print(paste('In if for',substr(dataread[1,1],1,10)))
		yr = as.character(dataread[1,1])
		mon = as.numeric(substr(yr,6,7))
		day = as.numeric(substr(yr,9,10))
		yr = as.numeric(substr(yr,1,4))
		if(yr > 2019 || (yr == 2019 && mon > 4) || (yr == 2019 && mon == 4 && day > 3))
			offsetCols = 2
    da = nrow(dataread)
    thresh = 5
    gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
    gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
    gsi3 = sum(dataread[complete.cases(dataread[,5]),5])/60000
		if(offsetCols)
		{
    	gsi4 = sum(dataread[complete.cases(dataread[,6]),6])/60000
    	gsi5 = sum(dataread[complete.cases(dataread[,7]),7])/60000
		}
    
		subdata = dataread[complete.cases(dataread[,3]),]
    if(nrow(subdata))
      subdata = subdata[as.numeric(subdata[,3]) > thresh,]
    
    if(length(dataread[complete.cases(dataread[,(7+offsetCols)]),(7+offsetCols)]))
      tamb = mean(dataread[complete.cases(dataread[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)]))
      tambst = mean(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,(6+offsetCols)]),(6+offsetCols)]))
      hamb = mean(dataread[complete.cases(dataread[,(6+offsetCols)]),(6+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(6+offsetCols)]),(6+offsetCols)]))
      hambst = mean(subdata[complete.cases(subdata[,(6+offsetCols)]),(6+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,(7+offsetCols)]),(7+offsetCols)]))
      tambmx = max(dataread[complete.cases(dataread[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)]))
      tambstmx = max(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,(6+offsetCols)]),(6+offsetCols)]))
      hambmx = max(dataread[complete.cases(dataread[,(6+offsetCols)]),(6+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)]))
      hambstmx = max(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,(7+offsetCols)]),(7+offsetCols)]))
      tambmn = min(dataread[complete.cases(dataread[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)]))
      tambstmn = min(subdata[complete.cases(subdata[,(7+offsetCols)]),(7+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,(6+offsetCols)]),(6+offsetCols)]))
      hambmn = min(dataread[complete.cases(dataread[,(6+offsetCols)]),(6+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(6+offsetCols)]),(6+offsetCols)]))
      hambstmn = min(subdata[complete.cases(subdata[,(6+offsetCols)]),(6+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,3]),(8+offsetCols)]))
      tsi1 = mean(dataread[complete.cases(dataread[,3]),(8+offsetCols)])
    
    if(length(dataread[complete.cases(dataread[,3]),(9+offsetCols)]))
      tsi2 = mean(dataread[complete.cases(dataread[,3]),(9+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(8+offsetCols)]),(8+offsetCols)]))
      tsi1st = mean(subdata[complete.cases(subdata[,(8+offsetCols)]),(8+offsetCols)])
    
    if(length(subdata[complete.cases(subdata[,(9+offsetCols)]),(9+offsetCols)]))
      tsi2st = mean(subdata[complete.cases(subdata[,(9+offsetCols)]),(9+offsetCols)])
    
    gsirat1 = gsi2 / gsi1
    gsirat2 = gsi3 / gsi1
    gsirat3 = gsi4 / gsi1
		gsirat4 = gsi5 / gsi1

    gmodwt = (gsi2*0.48)+(gsi3*0.52)
    
    rowa0 = as.numeric(dataread[,(41+offsetCols)])
    rowa0 = rowa0[complete.cases(rowa0)]
    {
      if(length(rowa0))
      {
        rowa0 = abs(rowa0)
        rowa0 = rowa0[rowa0 < 10000000]
        if(length(rowa0))
          rowa0 = round(abs(rowa0[1] - rowa0[length(rowa0)]),2)
      }
      else
      {
        rowa0 = NA
      }
    }
    rowcj = as.numeric(dataread[,(88+offsetCols)])
    rowcj = rowcj[complete.cases(rowcj)]
    {
      if(length(rowcj))
      {
        rowcj = abs(rowcj)
        rowcj = rowcj[rowcj < 10000000]
        if(length(rowcj))
        {
          rowcjlast = rowcj[length(rowcj)]
          rowcj = round(abs(rowcj[1] - rowcj[length(rowcj)]),2)
        }
      }
      else
        rowcj = NA
    }
    timelast = as.character(dataread[complete.cases(dataread[,(88+offsetCols)]),1])
    {
      if(length(timelast))
        timelast = timelast[length(timelast)]
      else
        timelast = NA
    }
    
    trans = abs(round((1 - (rowcj / rowa0))*100,2))
    if(!length(trans))
      trans = NA
    
    TSGHV = as.numeric(dataread[complete.cases(as.numeric(dataread[,(86+offsetCols)])),(86+offsetCols)])
    {
      if(length(TSGHV))
      {
        TSGHVTot = round((TSGHV[length(TSGHV)] - TSGHV[1]),2)
        TSGHV = TSGHV[TSGHV < 100000000]
        if(length(TSGHV))
          TSGHV = TSGHV[length(TSGHV)]
      }
      else
        TSGHV = NA
    }
    
    SCHV = as.numeric(dataread[complete.cases(as.numeric(dataread[,(85+offsetCols)])),(85+offsetCols)])
    {
      if(length(SCHV))
      {
        SCHVTot = round((SCHV[length(SCHV)] - SCHV[1]),2)
        SCHV = SCHV[SCHV < 100000000]
        if(length(SCHV))
          SCHV = SCHV[length(SCHV)]
      }
      else
        SCHV=NA
    }
    SiteLoss = round((as.numeric(SCHVTot)*100/as.numeric(TSGHVTot)),2)
    daycall =substr(dataread[1,1],1,10)
    
    ####
    
    dp=subset(dataread,dataread[,(16+offsetCols)]>=380 & dataread[,(16+offsetCols)]<=480,select = c(1,(16+offsetCols)))
    dp=separate(dp, col = Tm, into = c("Date", "time"), sep = " ")
    slv=length(which(as.character(dp[,2])>="07:00:00" & as.character(dp[,2])<="18:28:00"))-sum(is.na(dp[,3]))-length(which(dataread[,(18+offsetCols)]<=0))
    dp<-subset(dataread,dataread[,(63+offsetCols)]>=9000 & dataread[,(63+offsetCols)]<=12000,select = c(1,(63+offsetCols)))
    dp=separate(dp, col = Tm, into = c("Date", "time"), sep = " ")
    shv=length(which(as.character(dp[,2])>="07:00:00" & as.character(dp[,2])<="18:28:00"))-sum(is.na(dp[,3]))-length(which(dataread[,(18+offsetCols)]<=0))
   
    
    ga_lv=(slv*100)/(689-length(which(dataread[,(18+offsetCols)]<=0)))
    ga_hv=(shv*100)/(689-length(which(dataread[,(18+offsetCols)]<=0)))
  }
  #	print(paste(
  #	daycall , da , gs1 , gsi2 , gsi3 , tamb , tambst , tambmx , tambmn , tambstmx ,"\n",
  #	tambstmn , hamb , hambst , hambmx , hambmn , hambstmx , hambstmn , tsi1 , tsi2 , gsirat1 , gsirat2 ,"\n",
  #	rowa0 , rowcj , trans , rowcjlast , timelast , TSGHV , SCHV , TSGHVTot , SCHVTot , SiteLoss,"\n" ))
  hvyield = round(as.numeric(rowcj)/1814.4,2)
  lvyield = round(as.numeric(rowa0)/1814.4,2)
  prlv = rf1(as.numeric(lvyield)*100/as.numeric(gsi1))
  prlvmod = rf1(as.numeric(lvyield)*100/as.numeric(gmodwt))
  
  prhv = rf1(as.numeric(hvyield)*100/as.numeric(gsi1))
  prhvmod =rf1(as.numeric(hvyield)*100/as.numeric(gmodwt))
  daPerc = da/14.4
  datawrite = data.frame(
	Date = daycall,PtsRec = rf(da),Gsi = rf(gsi1),
	Gmod10N = rf(gsi2),Gmod10S = rf(gsi3),Tamb = rf1(tamb),
	TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
  TambSHmx = rf1(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb),
	HambSH = rf1(hambst), HambMx = rf1(hambmx), HambMn = rf1(hambmn),
	HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),TModN = rf1(tsi1), 
	TModS= rf1(tsi2), GModNRat = rf3(gsirat1), GModSRat = rf3(gsirat2),
  SolEnergyDelLVSide=rowa0,SolEnergyDelHVSide=rowcj,TransformerLoss = trans,
	LastReadingHVSide=rowcjlast,LastTime=timelast,LastReadTrueSolGenHV=TSGHV,
  LastReadSiteConsHV=SCHV,SolGenHV=TSGHVTot,SiteConsHV=SCHVTot,
  SiteLossCons=SiteLoss,TModNSH=rf1(tsi1st),TModSSH=rf1(tsi2st),
	GmodWT=rf2(gmodwt),GA_LV=rf2(ga_lv),GA_HV=rf2(ga_hv),
	HvYld = hvyield, LvYld = lvyield, PRLV = prlv, 
	PRLVMod = prlvmod, PRHV = prhv,PRHVMod = prhvmod,DA=rf1(daPerc),
	GMod10E = rf(gsi4), GMod10W = rf(gsi5),GModERat = rf3(gsirat3),GModWRat = rf3(gsirat4),
	stringsAsFactors=F)
  
	datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),Gsi = as.character(datawrite[1,3]),GModN = as.character(datawrite[1,4]),GModS = as.character(datawrite[1,5]),
                  Tamb = as.character(datawrite[1,6]),TambSH = as.character(datawrite[1,7]),Hamb = as.character(datawrite[1,12]),HambSH = as.character(datawrite[,13]),SolEnergyDelLVSide = as.character(datawrite[1,22]),SolEnergyDelHVSide=as.character(datawrite[1,23]),TransformerLoss=as.character(datawrite[1,24]),GmodWT=as.character(datawrite[1,34]),
									DA = as.character(datawrite[1,2]), PRLV = as.character(datawrite[1,39]),PRLVMod = as.character(datawrite[1,40]),
									PRHV = as.character(datawrite[1,41]), PRHVMod = as.character(datawrite[1,42]),GMod10E=as.character(datawrite[1,44]),GMod10W=as.character(datawrite[1,45]), LastReadTrueSolGenHV = as.character(datawrite[1,27]), LastReadSiteConsHV = as.character(datawrite[1,28]),
									stringsAsFactors=F)
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[712]"
pathwrite = "/home/admin/Dropbox/Second Gen/[IN-015S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    days = days[grepl("txt",days)]
    sumfilename = paste("[IN-015S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
			print(paste(pathmonth,days[z]),sep="/")
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
      daywr = gsub("712","IN-015S",days[z])
      write.table(datawrite,file = paste(writemonth,daywr,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
