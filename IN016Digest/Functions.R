################################################################################
#  Functions.R
# This file consists of the function definition of the modules used by Live.R
################################################################################

require('httr') # reuqired for POST function needed to fetch data from the server

source('/home/admin/CODE/MasterMail/timestamp.R')
#LISTOFSTATIONNAMES is a global array which stores the names of the stations
#for which we want to capture Gen-1 data
LISTOFSTATIONNAMES = c("DTVS-Mannur-DI","DTVS-Mannur-Production","DTVS-Mannur-Tech.Center","DTVS-Mannur-UPCR")

# cleansedata is a function that cleans the raw output of the POST function into
# the more condesed Gen-1 format
cleansedata = function(pth)
{
#read path containing all stations aggregated into one file
	dfall = read.table(pth,header = T,sep="\t")

	listofstrngs = LISTOFSTATIONNAMES
	listofdfs = list() #list which will store a list of data-frames, one per station
	for(t in 1 : length(listofstrngs))
	{
	  #search for occurance of listofstrngs[t] in dfall[13]  and correspondingly extract sub-data and store as a table.
		idxuse = which(as.character(dfall[,13]) %in% listofstrngs[t])
	  df = dfall[idxuse,]
    c2 = as.numeric(df[,17])
    c2idx = which(c2 %in% 0)
	  c1 = as.character(df[,19])
    c2 = as.character(df[,18])
    c3 = as.character(df[,17])
    if(length(c2idx) > 1)
	  {
	    c1 = as.character(df[-(c2idx),19])
      c2 = as.character(df[-(c2idx),18])
      c3 = as.character(df[-(c2idx),17])
    }
		tmLast = c1[length(c1)]
		recordTimeMaster("IN-016T","Gen1",as.character(tmLast))
    listofdfs[[t]] = data.frame(Tm=c1,Pac=c2,Eac=c3)
	}
	#df2 = data.frame(Tm=c1,Pac=c2,Eac=c3)
  return(listofdfs)
}

pathCentral = '/home/admin/Data/TORP Data/AllStations'

probePath = function(day)
{
	path = pathCentral
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	#checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	#checkDir(pathYr)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	print(paste('pathfinal',pathFinal))
	{
		if(file.exists(pathFinal))
		{
			df = read.table(pathFinal,header = T,sep = "\t")
			return(df)
		}
		else
			return(NULL)
	}
}

fetchrawdata = function(day1,day2)
{
  	#fetch data from SERVER USING A POST-request
		masterData = probePath(day1)
		
		if(is.null(masterData))
		{
			print('returning NULL')
			return(masterData)
		}

		stnnames = as.character(masterData[,13])
		subst = which(stnnames %in% LISTOFSTATIONNAMES)
		print(paste('length of subst',length(subst)))
		if(length(subst))
		{
			vals2 = masterData[subst,] # extract only rows with MJ Logistics tag in column 13
			recordTimeMaster("IN-016T","FTPNewFiles",as.character(vals2[nrow(vals2),19]))
			return(vals2)
		}
		print('returning NULL')
		return(NULL)
}

