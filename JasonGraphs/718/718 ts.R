#########################
rm(list=ls(all =TRUE))
library(ggplot2)
pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[718]"
setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)

dff=NULL
dffg=NULL

for(z in filelist[1:length(filelist)]){
  temp <- read.table(z, header =T, sep= '\t', stringsAsFactors = F)
  pacTemp <- temp
  #smp10
  irrad <- as.numeric(paste(abs(pacTemp[,3])))    
  
  #smp10 at 7 to 6pm with values above 5w/m2
  smp10_points <- abs(irrad[irrad>5])
  
  #smp10<- abs(irrad)
  timestamps <- cbind(pacTemp[,1],irrad)
  
  #GRID VOLTAGE
  
  ts <- data.frame(timestamps)
  print(paste(z, "done"))
}

dffg=data.frame(dffg)

#1:length(dffg[,2])

ts <- ggplot(data=dffg, aes(x=1:length(dffg[,2]),y=as.numeric(as.character(dffg[,2])))) + geom_point(color= "red", size = 0.9)
ts <- ts + theme_bw() + expand_limits(x =length(dff[,2]), y= c(0,800))
ts <- ts + ggtitle(paste0("[718] Time series plot for SPM10 ")) + ylab("W/m2") + xlab("Time ; condition of 7am-6pm daily")
ts <- ts + theme(plot.title = element_text(hjust = 0.5))
#ts <- ts + scale_x_date(date_breaks = "5 days",date_labels = "%b %d")

ts

ggsave(paste0('/home/admin/Jason/cec intern/results/718/2. time series.pdf'), ts, width =10, height=5)


